package com.liu.pojo ;


import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;

@Data
@Alias("Gouwuche")
public class Gouwuche {

  private Integer id;
  private BigDecimal count;
  private Product product;
  private Users users;
  private BigDecimal sum;



}
