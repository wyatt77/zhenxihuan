package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("wechatUser")
public class WechatUser {

  private String openid;
  private String token;
  private Integer uid;

}
