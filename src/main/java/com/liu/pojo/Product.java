package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import java.math.BigDecimal;

@Data
@Alias("Product")
public class Product {

    private Integer proId;//商品编号
    private String proName;//商品名称
    private String proPic;//商品图片
    private BigDecimal proPrice;//商品价格
    private BigDecimal discountPrice;//商品折扣价
    private Double discountRate;//商品折扣率
    private Integer inventory;//库存
    private Integer storeId;//外键关联店铺id
    private Integer proTypeId;//外键关联商品类型id
    private String proDetails;//商品详情
    private Date createTime;//创建时间
    private Date startTime;//上架时间
    private Date modifyTime;//最近修改时间
    private Date endTime;//下架时间
    private Integer proStatus;//产品状态

    private Store store;
    private ProductType productType;

}
