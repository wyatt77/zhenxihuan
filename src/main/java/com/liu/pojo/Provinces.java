package com.liu.pojo;

import lombok.Data;

@Data
public class Provinces {

  private Long id;
  private String provinceCode;
  private String provinceName;

}
