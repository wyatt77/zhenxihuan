package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Data
@Alias("OrderDetaill")
public class OrderDetaill {

  private Long id;
  private Long productId;
  private Long orderNum;
  private Long count;
  private Date creatTime;
  private Double price;

}
