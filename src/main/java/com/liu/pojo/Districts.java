package com.liu.pojo;

import lombok.Data;

@Data
public class Districts {

  private Long id;
  private String areaCode;
  private String areaName;
  private Long cityId;

}
