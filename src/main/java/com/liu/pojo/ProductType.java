package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("ProductType")
public class ProductType {

    private Integer proTypeId;
    private String proTypeName;

}
