package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Data
@Alias("administrators")
public class Administrators {

  private Integer adminId;
  private String adminName;
  private String username;
  private String password;
  private String address;
  private String adminPhone;
  private Date createTime;
  private Date modifyTime;
  private Integer status;

}
