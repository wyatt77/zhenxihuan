package com.liu.pojo;

import lombok.Data;

@Data
public class Address {

  private Long addrId;
  private Long provId;
  private Long cityId;
  private Long distrId;
  private String detailedAddress;
  private Long defaultAddr;
  private Long userId;
  private Provinces provinces;
  private Cities cities;
  private Districts districts;
  private Users users;

}
