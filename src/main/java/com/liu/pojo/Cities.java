package com.liu.pojo;

import lombok.Data;

@Data
public class Cities {

  private Long id;
  private String cityCode;
  private String cityName;
  private Long provinceId;

}
