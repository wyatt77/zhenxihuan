package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("OrderStatus")
public class OrderStatus {
    private String value;
    private String name;
}
