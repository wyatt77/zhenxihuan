package com.liu.pojo;

import lombok.Data;

@Data
public class Store {

  private long storeId;
  private String storeName;
  private String storePhone;
  private String storeLicenseNum;
  private String storeLicensePicture;
  private long bossId;
  private String address;
  private java.sql.Timestamp createTime;
  private java.sql.Timestamp deleteTime;
  private java.sql.Timestamp modifyTime;

}
