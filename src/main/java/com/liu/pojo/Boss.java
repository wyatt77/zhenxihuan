package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;

@Data
@Alias("boss")
public class Boss {

  private Long bossId;
  private String username;
  private String password;
  private String phone;
  private String qq;
  private String weChat;
  private String address;
  private Date createTime;
  private Date modifyTime;
  private Long status;

}
