package com.liu.pojo;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.ibatis.type.Alias;


import java.util.Date;

@Data
@Alias("order")
public class Order{

    private Integer  id;//订单id
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss" ,timezone = "GMT+8")
    private Date     orderTime;//订货时间
    private Date     payTime;//付款时间
    private Date     acceptTime;
    private Date     refundTime;
    private long     orderNum;
    private long     wxpayNum;
    private Double   totalPrice;
    private OrderStatus orderStatus;
    private Integer  shopId;
    private Integer  customerId;
    private Double   postMoney;
    private String   provinceCode;
    private String   cityCode;
    private String   areaCode;
    private String   address;
    private String   remark;
    private Double   reallyPrice;
    private String   consigneeTel;
    private String   consigneeName;
    private Integer  laundryMethod;

}
