package com.liu.pojo;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.Date;
import java.io.Serializable;

@Data
@Alias("users")
public class Users implements Serializable {

  private Integer userId;
  private String username;
  private String password;
  private String token;
  private String nickName;
  private String role;
  private Long gender;
  private Date createDate;
  private String name;

}
