package com.liu.pojo;

import lombok.Data;

@Data
public class ProductCopy1 {

  private long proId;
  private String proName;
  private double proPrice;
  private double discountPrice;
  private double discountRate;
  private long inventory;
  private long storeId;
  private long proTypeId;
  private String proDetails;
  private java.sql.Timestamp createTime;
  private java.sql.Timestamp modifyTime;
  private java.sql.Timestamp deleteTime;


}
