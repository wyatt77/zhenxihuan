package com.liu;


import com.liu.mapper.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
       //权限列表

        com.liu.pojo.Users
                myuser = usersMapper.find_user(s);
        List<GrantedAuthority> authorities= new ArrayList<GrantedAuthority>();

        authorities.add(new SimpleGrantedAuthority(s));
        String password = "";
        if(myuser!=null){
            password = myuser.getPassword();
        }
        //System.out.println(myuser.getPwd());
        User user  =
                new User(s,
                        password, true,
                        true,
                        true,
                        true, authorities);

        return user;
    }
}
