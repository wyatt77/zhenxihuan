package com.liu.util;



        import java.util.List;
        import java.util.Map;

/**
 * Created by Administrator on 2019/2/7.
 */
public class Result<T> {
    private Long total;
    private List<T> items;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
