package com.liu.util;

import java.util.HashMap;
import java.util.Map;

public enum MsgEnum {
    SUCCESS("1","成功"),
    FAILED("-1","失败"),
    EMPTY("0","入口参数为空");
    private final String code;
    private  final String  msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    private MsgEnum(final String code, final String msg) {
        this.code = code;
        this.msg=msg;
    }


}
