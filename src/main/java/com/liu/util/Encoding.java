package com.liu.util;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * 加密类：主要使用MD5加密
 */
public class Encoding {

    /**
     * MD5加密方法
     *
     * @param string ：要加密的字符串
     * @return 加密后的字符串，转大写
     */
    public static String encoding(String string) {

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(string.getBytes());
            BigInteger bigInteger = new BigInteger(1, messageDigest.digest());
            return bigInteger.toString(16).toUpperCase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(encoding("admin123"));
        //admin123:192023A7BBD73250516F069DF18B500
    }

}
