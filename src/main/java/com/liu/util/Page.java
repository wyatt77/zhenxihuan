package com.liu.util;

import java.util.List;

public class Page<T> {
    /**
     * pageNum:当前页码
     * rowNum:起始行号
     * totalPages:总页数
     * totalCount:总条数
     * pageCount:当前页几条
     * list:当前页的内容
     */
    private Integer pageNum;
    private Integer rowNum;
    private Integer totalPages;
    private Integer totalCount;
    private Integer pageCount = 5;
    private List<T> list;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        if (pageNum == null) {
            pageNum = 1;
        }
        if (pageNum <= 0) {
            pageNum = 1;
        }
        if (pageNum >= totalPages && totalPages > 0) {
            pageNum = totalPages;
        }
        this.pageNum = pageNum;
    }

    public Integer getRowNum() {
        return rowNum;
    }

    public void setRowNum() {
        this.rowNum = (pageNum - 1) * pageCount;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages() {
        this.totalPages = (totalCount % pageCount == 0) ? (totalCount / pageCount) : (totalCount / pageCount + 1);
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        if (pageCount == null) {
            pageCount = 10;
        }
        if (pageCount <= 0) {
            pageCount = 10;
        }
        this.pageCount = pageCount;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNum=" + pageNum +
                ", rowNum=" + rowNum +
                ", totalPages=" + totalPages +
                ", totalCount=" + totalCount +
                ", pageCount=" + pageCount +
                ", list=" + list +
                '}';
    }

    //必须使用此顺序，初始化当前的对象
    public void init(Integer pageNum, Integer pageCount, Integer totalCount) {
        setTotalCount(totalCount);
        setPageCount(pageCount);
        setTotalPages();
        setPageNum(pageNum);
        setRowNum();
    }
}
