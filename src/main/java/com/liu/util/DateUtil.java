package com.liu.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static int getAge(Date date) {
        if (date == null) {
            return 0;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //2000
        int year = calendar.get(Calendar.YEAR);
        Calendar now = Calendar.getInstance();
        //2019
        int nowYear = now.get(Calendar.YEAR);
        return nowYear - year == 0 ? 1 : nowYear - year;
    }

    public static Date getBirthday(String date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date birthday = null;
        try {
            birthday = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return birthday;
    }

    public static String fomartBirthday(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String birthday = format.format(date);
        return birthday;
    }
}
