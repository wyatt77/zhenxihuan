package com.liu.base;

import com.liu.util.MsgEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;


public abstract class  BaseController {
    private final Logger logger= LoggerFactory.getLogger(getClass());
    protected Map<String,Object> resulMap(MsgEnum result, Object pd){
     Map<String,Object> map=new HashMap<>();
     map.put("code",result.getCode());
     map.put("msg",result.getMsg());
     map.put("data",pd);
     return  map;
    }
    protected Map<String,Object> resulMap(MsgEnum result, Object pd,Object other){
        Map<String,Object> map=new HashMap<>();
        map.put("code",result.getCode());
        map.put("msg",result.getMsg());
        map.put("data",pd);
        map.put("other",other);
        return  map;
    }
    protected void logbefor(){
        logger.info("方法开始......");
    }
    protected void logafter(){
        logger.info("方法结束......");
    }
}
