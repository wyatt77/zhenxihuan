package com.liu.mapper;

import com.liu.pojo.Gouwuche;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface GouWuCheMapper{

    @Select("SELECT count(1) FROM gouwuche")
    int getSum();

    List<Gouwuche> findGouWuChe(@Param("startRowNum") Integer startRowNum,
                                @Param("endRowNum") Integer endRowNum);


    @Update("UPDATE gouwuche SET count =#{count} WHERE id=#{id}")
    Long updateGouWuChe(@Param("count") Long count,@Param("id")  Integer id);

    @Delete("DELETE FROM gouwuche WHERE Id =#{id} ")
    Long delestGouWuChe(@Param("id") Long id);
}
