package com.liu.mapper;

import com.liu.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 商品mapper接口
 * 定义增删改查功能的抽象方法
 */
@Mapper
public interface ProductMapper {
    //增加
    //删除
    //修改
    //查看
    long count_all_product(Map map);


    List<Product> find_all_product(Map map);
    /**
     * 顾客：获取所有的商品
     *
     * @param storeId
     * @return
     */
    List<Product> getAllProductList(@Param("storeId") Integer storeId,
                                    @Param("proStatus") Integer proStatus,
                                    @Param("pageNum") Integer pageNum,
                                    @Param("pageSize") Integer pageSize);

    /**
     * 商家：添加商品
     *
     * @param product
     * @return
     */
    Integer addProduct(Product product);

    /**
     * 商家：修改商品信息
     *
     * @param product
     * @return
     */
    Integer updateProduct(Product product);

    /**
     * 商家：下架商品
     *
     * @param proId
     * @return
     */
    Integer xiajiaProduct(Integer proId);

    /**
     * @param proId
     * @return
     */
    Integer shangjiaProduct(Integer proId);

    /**
     * 商家：删除商品
     *
     * @param proId
     * @return
     */
    Integer deleteProduct(Integer proId);

}


