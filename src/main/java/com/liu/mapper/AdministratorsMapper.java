package com.liu.mapper;

import com.liu.pojo.Administrators;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdministratorsMapper {
    //查询用户总数
    int getSum(String username,Integer status);

    //根据id进行分页
    List<Administrators> getAdmin(
            @Param("startRowNum") Integer startRowNum,
            @Param("endRowNum") Integer endRowNum,
            @Param("username")String username,
            @Param("status")Integer status);

    //添加用户
    Long addAdmin(Administrators administrators);

    //根据id删除用户
    Integer delAdminById(@Param("id") Integer id);

    //修改用户
    Long updateAdmin(Administrators administrators);

}
