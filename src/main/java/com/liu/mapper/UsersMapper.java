package com.liu.mapper;

import com.liu.pojo.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UsersMapper {
    //查询用户总数
    int getSum(String username,String role);

    //根据id进行分页
    List<Users> getUsers(
            @Param("startRowNum") Integer startRowNum,
            @Param("endRowNum") Integer endRowNum,
            @Param("username") String username,
            @Param("role") String role);

    //添加用户
    Long addUsers(Users users);

    //根据id删除用户
    Integer delUsersById(@Param("id") Integer id);

    //修改用户
    Long updateUsers(Users users);

    //查询用户(id)
    List<Users> getUsersById(@Param("id") Long id);

    //查询用户(模糊查询)
    List<Users> getUsersByName(@Param("username") String username);

    //登录
    Users find_user(@Param("nickName") String nickName);

    int add_user(Map map);
    String is_user_openid_repeat(String openid);
    int update_user(Map map);
    int get_uid(Map map);


}
