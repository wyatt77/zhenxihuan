package com.liu.mapper;

import com.liu.pojo.Order;
import com.liu.pojo.OrderStatus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: zhenxihuan
 * @description
 * @author: zlr
 * @create: 2019-07-24 22:05
 **/
@Mapper
public interface OrderMapper{
    void insetOrder(@Param(value = "order") Order order);

    void delorder(Integer id);

    void updateStatus(@Param(value="id") Integer id, @Param(value="status") Integer status);

    List<Order> list();

    List<OrderStatus> statusList();
}
