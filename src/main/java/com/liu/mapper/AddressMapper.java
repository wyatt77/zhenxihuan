package com.liu.mapper;

import com.liu.pojo.Address;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface AddressMapper {

    List<Address> getAllAddress();

    long delAddressById(@Param("id") Long id);


    long addAddress(@Param("prov_id") Long prov_id, @Param("city_id")Long city_id,
                    @Param("distr_id")Long distr_id, @Param("user_id")Long user_id,
                    @Param("detailed_address") String detailed_address,
                    @Param("default_addr") Long default_addr);
}
