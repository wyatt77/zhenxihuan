package com.liu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.liu.pojo.Administrators;
import com.liu.service.AdministratorsService;
import com.liu.util.DateUtil;
import com.liu.util.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(
        methods = {
                RequestMethod.GET,
                RequestMethod.POST,
                RequestMethod.DELETE,
                RequestMethod.HEAD,
                RequestMethod.PUT,
                RequestMethod.OPTIONS,
                RequestMethod.TRACE},
        allowedHeaders = {"*"},
        maxAge = 50000,
        origins = {"*"}
)
public class AdministratorsController {
    @Resource
    AdministratorsService administratorsService;

    @RequestMapping(value = "/getAdminByPage.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String adminList(@RequestBody Map map) {
        Integer pageNum = (Integer) map.get("pageNum");
        Integer pageCount = (Integer) map.get("limit");
        String username = (String) map.get("title");
        String status = (String) map.get("importance");
        Page<Administrators> page = administratorsService.getAdminByPage(pageNum, pageCount,username,status);
        Map<String, Object> m = new HashMap<>();
        m.put("page", page);
        return JSON.toJSONStringWithDateFormat(m, "yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

    @RequestMapping(value = "/updateAdmin.html", method = {RequestMethod.GET, RequestMethod.POST})
    public Long update(@RequestBody Map map) {
        Administrators administrators = new Administrators();
        administrators.setAdminId((Integer) map.get("adminId"));
        administrators.setAdminName((String) map.get("adminName"));
        administrators.setUsername((String) map.get("username"));
        administrators.setPassword((String) map.get("password"));
        administrators.setAddress((String) map.get("address"));
        administrators.setAdminPhone((String) map.get("adminPhone"));
        administrators.setCreateTime(DateUtil.getBirthday((String) map.get("createTime")));
        administrators.setModifyTime(DateUtil.getBirthday((String) map.get("modifyTime")));
        administrators.setStatus((Integer) map.get("status"));
        return administratorsService.updateAdmin(administrators);
    }

    @RequestMapping(value = "/delAdminById.html", method = {RequestMethod.GET, RequestMethod.POST}, produces = "text/html;charset=utf-8")
    public String delAdminById(@RequestBody Map map) {
        Integer id = (Integer) map.get("id");
        Integer num = administratorsService.delAdminById(id);
        return JSON.toJSONString(num);
    }

    @RequestMapping(value = "/addAdmin.html", method = {RequestMethod.GET, RequestMethod.POST})
    public Long addAdmin(@RequestBody Map map) {
        Administrators administrators = new Administrators();
        administrators.setAdminId((Integer) map.get("adminId"));
        administrators.setAdminName((String) map.get("adminName"));
        administrators.setUsername((String) map.get("username"));
        administrators.setPassword((String) map.get("password"));
        administrators.setAddress((String) map.get("address"));
        administrators.setAdminPhone((String) map.get("adminPhone"));
        administrators.setCreateTime(DateUtil.getBirthday((String) map.get("createTime")));
        administrators.setModifyTime(DateUtil.getBirthday((String) map.get("modifyTime")));
        administrators.setStatus((Integer) map.get("status"));
        return administratorsService.addAdmin(administrators);
    }
}
