package com.liu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.liu.pojo.Product;
import com.liu.service.ProductService;
import com.liu.util.FileUtil;
import com.liu.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/all")
    public String getAllProductList(@RequestBody Map<String, Integer> map) {
        Integer storeId = (Integer) map.get("storeId");
        Integer proStatus = (Integer) map.get("proStatus");
        Integer pageNum= (Integer) map.get("pageNum");
        Integer pageSize= (Integer) map.get("pageSize");
        return JSON.toJSONStringWithDateFormat(productService.getAllProductList(storeId, proStatus,pageNum,pageSize), "yyyy-MM-dd HH:mm:ss", SerializerFeature.WriteDateUseDateFormat);
        //return JSON.toJSONString(productService.getAllProductList(storeId, proStatus));
    }

    @PostMapping("/add")
    public Integer addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @PostMapping("/del")
    public Integer deleteProduct(@RequestBody Integer proId) {
        return productService.deleteProduct(proId);
    }

    @PostMapping("/update")
    public Integer updateProduct(@RequestBody Product product) {
        return productService.updateProduct(product);

    }

    @PostMapping("/shangjia")
    public Integer shangjiaProduct(@RequestBody Integer proId) {
        return productService.shangjiaProduct(proId);

    }

    @PostMapping("/xiajia")
    public Integer xiajiaProduct(@RequestBody Integer proId) {
        return productService.xiajiaProduct(proId);

    }


    @PostMapping("/find_all_product")
    public Result<Product> find_all_product(@RequestBody Map map){
        Result<Product> result = new Result<>();
        List<Product> rlist = productService.find_all_product(map);
        System.out.println(rlist.size());
        result.setItems(rlist);
        result.setTotal(productService.count_all_product(map));
        return result;
    }


    // 到底什么东西需要彻底弄懂，什么东西不用搞懂
    // 底层原理（TCP/IP 请求头/响应头）
    @GetMapping(value ="/upload",produces="text/javascript;charset=utf-8")
    public String configUpload(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String action = req.getParameter("action");
        if(action!=null){
            String callback = req.getParameter("callback");
            String s = FileUtil.readFileAsString("c://ueditorconfig.txt");
            return callback+"("+s+")";
        }
        return null;
    }

    @PostMapping("/upload")
    public Map handleFileUpload(String token,@RequestParam("file") MultipartFile file) {
        String oldFileName = file.getOriginalFilename();
        int lastDotIndex = oldFileName.lastIndexOf(".");
        String extName = oldFileName.substring(lastDotIndex);
        String newName =  UUID.randomUUID()+extName;
        String os = System.getProperty("os.name");

        File excelFile = new File((os.startsWith("Windows")
                ?"c://":"/upload/") + newName);
        // System.out.println(excelFile.getAbsolutePath());
        try {
            file.transferTo(excelFile);
            Map map = new HashMap();
            map.put("original",oldFileName);
            map.put("size",file.getSize());
            map.put("state", "SUCCESS");
            map.put("title", newName);
            map.put("type", extName);
            map.put("url", "/find_img?fileName="+newName+"&token="+token);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
