package com.liu.controller;


import com.liu.mapper.AddressMapper;
import com.liu.pojo.Address;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class AddressController {
    @Resource
    AddressMapper addressMapper;

    @RequestMapping("getAllAddress")
    public List<Address> getAllAddress(){
        List list = addressMapper.getAllAddress();
        System.out.println(list);
        return list;
    }
    @RequestMapping("delAddressById")
    public long delAddressById(@RequestBody Map map){
        Long id = Long.valueOf((Integer)map.get("id"));
        System.out.println(id);
        return addressMapper.delAddressById(id);
    }
    @RequestMapping("addAddress")
    public long addAddress(@RequestBody Map map){
        Long prov_id = Long.valueOf((Integer)map.get("prov_id"));
        Long city_id = Long.valueOf((Integer)map.get("city_id"));
        Long distr_id = Long.valueOf((Integer)map.get("distrid"));
        Long user_id = Long.valueOf((Integer)map.get("user_id"));
        String detailed_address = (String)map.get("detailed_address");
        Long default_addr = Long.valueOf((Integer)map.get("default_addr"));
        Long num =addressMapper.addAddress(prov_id,city_id,distr_id,user_id,detailed_address,default_addr);
        System.out.println(num);
        return num;
    }

}
