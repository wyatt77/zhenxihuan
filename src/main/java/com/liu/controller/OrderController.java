package com.liu.controller;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.liu.base.BaseController;
import com.liu.pojo.Order;
import com.liu.pojo.OrderStatus;
import com.liu.service.OrderService;
import com.liu.util.MsgEnum;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @program: zhenxihuan
 * @description
 * @author: zlr
 * @create: 2019-07-24 21:52
 **/
@RequestMapping("/order")
@RestController
@CrossOrigin(
        methods = {
                RequestMethod.GET,
                RequestMethod.POST,
                RequestMethod.DELETE,
                RequestMethod.HEAD,
                RequestMethod.PUT,
                RequestMethod.OPTIONS,
                RequestMethod.TRACE},
        allowedHeaders = {"*"},
        maxAge = 50000,
        origins = {"*"}
)
@Slf4j
public class OrderController extends BaseController {
    @Autowired
    private OrderService orderService;

    /**
     * 增加订单
     *
     * @param order
     * @return
     */

    @PostMapping("/addOrder")
    public Map<String, Object> addOrder(Order order) {
          logbefor();
        MsgEnum emum=null;
        try {
            orderService.addorder(order);
            emum=MsgEnum.SUCCESS;
        } catch (Exception e){
             emum=MsgEnum.FAILED;
        }finally {
            logafter();
            return this.resulMap(emum, null);
        }

    }


    /**
     * 订单删除
     *
     * @param map
     * @return
     */
    @PostMapping("/delOrder")
    public Map<String, Object> delOrder(@RequestBody Map  map) {
        if (map.isEmpty() || !map.containsKey("id") || null ==map.get("id"))
            return this.resulMap(MsgEnum.EMPTY, null);
        try {
             Integer id = Integer.parseInt(map.get("id").toString());
             orderService.delorder(id);
            return this.resulMap(MsgEnum.SUCCESS, null);
        } catch (Exception e){
            return this.resulMap(MsgEnum.FAILED, null);
        }

    }

    /**
     * 订单审核操作（更改状态）
     * @param map
     * @return
     */
    @PostMapping("/updateStatus")
    public Map<String, Object> updateStatus(@RequestBody Map  map) {
        if (map.isEmpty() || !map.containsKey("id")
                || !map.containsKey("status") || null ==map.get("id"))
            return this.resulMap(MsgEnum.EMPTY, null);
        try {
            Integer id = Integer.parseInt(map.get("id").toString());
            Integer status = Integer.parseInt(map.get("status").toString());
            orderService.updateStatus(id, status);
            return this.resulMap(MsgEnum.SUCCESS, null);
        }catch (Exception e){
            return this.resulMap(MsgEnum.FAILED, null);
        }

    }


    /**
     * 订单分页列表
     *
     * @param pageNum
     * @param pagesize
     * @return
     */
    @PostMapping("/orderList")
    public Map<String, Object> orderList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pagesize", required = false, defaultValue = "30") Integer pagesize) {
       try{
           PageHelper.startPage(pageNum, pagesize);
           List<Order> list = orderService.orderList();
           Page<Order> orderPage = (Page<Order>) list;
           List<OrderStatus> statusList=orderService.statusList();
           return this.resulMap(MsgEnum.SUCCESS,orderPage,statusList);
       }catch (Exception e){
           return this.resulMap(MsgEnum.FAILED, null);
       }

    }
}
