package com.liu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.liu.mapper.GouWuCheMapper;
import com.liu.pojo.Gouwuche;
import com.liu.service.GouWuCheService;
import com.liu.util.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin(
        methods = {
                RequestMethod.GET,
                RequestMethod.POST,
                RequestMethod.DELETE,
                RequestMethod.HEAD,
                RequestMethod.PUT,
                RequestMethod.OPTIONS,
                RequestMethod.TRACE},
        allowedHeaders = {"*"},
        maxAge = 50000,
        origins = {"*"}
)
public class GouWuCheController {

    @Resource
    private  GouWuCheMapper gouWuCheMapper;
    @Resource
    private GouWuCheService gouWuCheService;

    @PostMapping("/findGouWuChe")
    public String findGouWuChe(@RequestBody Map map){
        Integer pageNum = (Integer) map.get("page");
        Integer pageCount = (Integer) map.get("limit");
        Page<Gouwuche> page=gouWuCheService.getGouWuCheByPage(pageNum,pageCount);
        for (int i=0;i<page.getList().size();i++){
            Gouwuche gouwuche=page.getList().get(i);
            BigDecimal count=gouwuche.getCount();
            BigDecimal price=gouwuche.getProduct().getProPrice();
            BigDecimal sum=count.multiply(price);
            gouwuche.setSum(sum);
            page.getList().set(i,gouwuche);
        }
        Map<String,Object> map1=new HashMap<>();
        map1.put("page",page);
        return JSON.toJSONStringWithDateFormat(map1,"yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

    @PostMapping("/updateGouWuChe")
    public String updateGouWuChe(@RequestBody Map map){
        Long count=Long.valueOf((String) map.get("count"));
        Integer id=(Integer) map.get("id");
        Long num=gouWuCheMapper.updateGouWuChe(count,id);
        return JSON.toJSONString(num);
    }

    @PostMapping("/deleteGouWuChe")
    public String deleteGouWuChe(@RequestBody Map map){
        Long id=Long.valueOf((Integer)map.get("id"));
        Long num=gouWuCheMapper.delestGouWuChe(id);
        return JSON.toJSONStringWithDateFormat(num,"yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

}
