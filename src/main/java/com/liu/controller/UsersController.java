package com.liu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.liu.pojo.Users;
import com.liu.service.UsersService;
import com.liu.util.DateUtil;
import com.liu.util.Page;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(
        methods = {
                RequestMethod.GET,
                RequestMethod.POST,
                RequestMethod.DELETE,
                RequestMethod.HEAD,
                RequestMethod.PUT,
                RequestMethod.OPTIONS,
                RequestMethod.TRACE},
        allowedHeaders = {"*"},
        maxAge = 50000,
        origins = {"*"}
)
public class UsersController {
    @Resource
    UsersService usersService;

    @RequestMapping(value = "/getUsersByPage.html", method = {RequestMethod.GET, RequestMethod.POST})
    public String userList(@RequestBody Map map) {
        Integer pageNum = (Integer) map.get("page");
        Integer pageCount = (Integer) map.get("limit");
        String username = (String) map.get("title");
        String role = (String) map.get("importance");
        Map<String, Object> m = new HashMap<>();
        Page<Users> page = usersService.getUsersByPage(pageNum, pageCount,username,role);
        m.put("page", page);
        return JSON.toJSONStringWithDateFormat(m, "yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }

    @RequestMapping(value = "/updateUsers.html", method = {RequestMethod.GET, RequestMethod.POST})
    public Long update(@RequestBody Map map) {
        Users users = new Users();
        users.setUserId((Integer) map.get("userId"));
        users.setRole((String) map.get("role"));
        users.setPassword((String) map.get("password"));
        users.setUsername((String) map.get("username"));
        users.setName((String) map.get("name"));
        if (map.get("gender").equals("男")){
            users.setGender(1L);
        }else if (map.get("gender").equals("女")){
            users.setGender(0L);
        }
        users.setCreateDate(DateUtil.getBirthday((String) map.get("createDate")));
        return usersService.updateUsers(users);
    }

    @RequestMapping(value = "/delUsersById.html", method = {RequestMethod.GET, RequestMethod.POST}, produces = "text/html;charset=utf-8")
    public String usersDelete(@RequestBody Map map) {
        Integer id = (Integer) map.get("id");
        Integer num = usersService.delUsersById(id);
        return JSON.toJSONString(num);
    }

    @RequestMapping(value = "/addUsers.html", method = {RequestMethod.GET, RequestMethod.POST})
    public Long usersAdd(@RequestBody Map map) {
        Users users = new Users();
        users.setUserId((Integer) map.get("userId"));
        users.setRole((String) map.get("role"));
        users.setPassword((String) map.get("password"));
        users.setUsername((String) map.get("username"));
        users.setName((String) map.get("name"));
        if (map.get("gender").equals("男")){
            users.setGender(1L);
        }else if (map.get("gender").equals("女")){
            users.setGender(0L);
        }
        users.setCreateDate(DateUtil.getBirthday((String) map.get("createDate")));
        return usersService.addUsers(users);
    }
}
