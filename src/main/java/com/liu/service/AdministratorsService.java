package com.liu.service;

import com.liu.pojo.Administrators;
import com.liu.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdministratorsService {
    //查询用户总数
    int getSum(String username,Integer status);

    //分页查询
    List<Administrators> getAdmin(
            @Param("startRowNum") Integer startRowNum,
            @Param("endRowNum") Integer endRowNum,
            @Param("username")String username,
            @Param("status")Integer status);

    Page<Administrators> getAdminByPage(Integer pageNum, Integer pageCount,
                                        String username,String status);

    //添加管理员
    Long addAdmin(Administrators administrators);

    //删除管理员
    Integer delAdminById(@Param("id") Integer id);

    //修改管理员
    Long updateAdmin(Administrators administrators);

}
