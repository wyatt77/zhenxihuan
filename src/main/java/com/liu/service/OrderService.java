package com.liu.service;

import com.liu.pojo.Order;
import com.liu.pojo.OrderStatus;

import java.util.List;

/**
 * @program: zhenxihuan
 * @description
 * @author: zlr
 * @create: 2019-07-24 22:04
 **/

public interface OrderService {

    void addorder(Order order);

    void delorder(Integer id);

    void updateStatus(Integer id, Integer status);

    List<Order> orderList();

    List<OrderStatus> statusList();
}
