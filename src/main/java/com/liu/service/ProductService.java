package com.liu.service;

import com.liu.pojo.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {


    long count_all_product(Map map);



    List<Product> find_all_product(Map map);

    /**
     * 顾客：获取所有的商品
     *
     * @param storeId
     * @return
     */
    List<Product> getAllProductList(Integer storeId,
                                    Integer proStatus,
                                    Integer pageNum,
                                    Integer pageSize);

    /**
     * 商家：添加商品
     *
     * @param product
     * @return
     */
    Integer addProduct(Product product);

    /**
     * 商家：修改商品信息
     *
     * @param product
     * @return
     */
    Integer updateProduct(Product product);

    /**
     * 商家：下架商品
     *
     * @param proId
     * @return
     */
    Integer shangjiaProduct(Integer proId);

    /**
     * 商家：下架商品
     *
     * @param proId
     * @return
     */
    Integer xiajiaProduct(Integer proId);

    /**
     * 商家：删除商品
     *
     * @param proId
     * @return
     */
    Integer deleteProduct(Integer proId);

}
