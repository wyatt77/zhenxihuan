package com.liu.service;

import com.liu.pojo.Users;
import com.liu.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UsersService {
    //查询用户总数
    int getSum(String username,String role);

    //分页查询
    List<Users> getUsers(
            @Param("startRowNum") Integer startRowNum,
            @Param("endRowNum") Integer endRowNum,
            @Param("username")String username,
            @Param("role")String role);

    Page<Users> getUsersByPage(Integer pageNum, Integer pageCount,
                               String username, String role);

    //添加用户
    Long addUsers(Users users);

    //删除用户
    Integer delUsersById(@Param("id") Integer id);

    //修改用户
    Long updateUsers(Users users);

}
