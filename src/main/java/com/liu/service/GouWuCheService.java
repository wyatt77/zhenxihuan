package com.liu.service;

import com.liu.pojo.Gouwuche;
import com.liu.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GouWuCheService {
    int getSum();

    List<Gouwuche> getGouWuChe(@Param("startRowNum") Integer startRowNum,
                               @Param("endRowNum") Integer endRowNum);

    Page<Gouwuche> getGouWuCheByPage(Integer pageNum, Integer pageCount);
}
