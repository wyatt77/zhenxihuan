package com.liu.service.impl;

import com.liu.mapper.ProductMapper;
import com.liu.pojo.Product;
import com.liu.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("productService")
@Transactional
public class ProductServiceImp implements ProductService {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public long count_all_product(Map map) {
        return productMapper.count_all_product(map);
    }

    ;

    @Override
    public List<Product> find_all_product(Map map) {
        return productMapper.find_all_product(map);
    }

    /**
     * 顾客：获取所有的商品
     *
     * @param storeId
     * @return
     */
    @Override
    public List<Product> getAllProductList(Integer storeId,
                                           Integer proStatus,
                                           Integer pageNum,
                                           Integer pageSize) {
        return productMapper.getAllProductList(storeId, proStatus, pageNum, pageSize);
    }

    /**
     * 商家：添加商品
     *
     * @param product
     * @return
     */
    @Override
    public Integer addProduct(Product product) {
        return productMapper.addProduct(product);
    }

    /**
     * 商家：修改商品信息
     *
     * @param product
     * @return
     */
    @Override
    public Integer updateProduct(Product product) {
        return productMapper.updateProduct(product);
    }

    /**
     * 商家：下架商品
     *
     * @param proId
     * @return
     */
    @Override
    public Integer shangjiaProduct(Integer proId) {
        return productMapper.shangjiaProduct(proId);
    }

    /**
     * 商家：下架商品
     *
     * @param proId
     * @return
     */
    @Override
    public Integer xiajiaProduct(Integer proId) {
        return productMapper.xiajiaProduct(proId);
    }

    /**
     * 商家：下架商品
     *
     * @param proId
     * @return
     */
    @Override
    public Integer deleteProduct(Integer proId) {
        return productMapper.deleteProduct(proId);
    }
}
