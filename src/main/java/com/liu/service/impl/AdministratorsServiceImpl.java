package com.liu.service.impl;

import com.liu.mapper.AdministratorsMapper;
import com.liu.pojo.Administrators;
import com.liu.service.AdministratorsService;
import com.liu.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("administrators")
public class AdministratorsServiceImpl implements AdministratorsService {
    @Resource
    private AdministratorsMapper administratorsMapper;

    @Override
    public int getSum(String username, Integer status) {
        return administratorsMapper.getSum(username, status);
    }

    @Override
    public List<Administrators> getAdmin(Integer startRowNum, Integer endRowNum, String username, Integer status) {
        List<Administrators> list = administratorsMapper.getAdmin(startRowNum, endRowNum, username, status);
        for (Administrators administrators : list) {
            System.out.println(administrators.toString());
        }
        return list;
    }

    @Override
    public Page<Administrators> getAdminByPage(Integer pageNum, Integer pageCount, String username, String status) {
        Integer num = 1;
        num = pageNum;
        Integer count = 10;
        count = pageCount;
        Integer s;
        switch (status) {
            case "运营良好": {
                s = 1;
            }
            break;
            case "濒临破产": {
                s = 2;
            }
            break;
            default: {
                s = null;
                break;
            }
        }
        Page<Administrators> page = new Page<>();
        page.init(num, count, getSum(username, s));
        page.setList(getAdmin(page.getRowNum(), count, username, s));
        return page;
    }

    @Override
    public Long addAdmin(Administrators administrators) {
        if (administrators == null) {
            return 0L;
        }
        long id = administratorsMapper.addAdmin(administrators);
        return id;
    }

    @Override
    public Integer delAdminById(Integer id) {
        return administratorsMapper.delAdminById(id);
    }

    @Override
    public Long updateAdmin(Administrators administrators) {
        return administratorsMapper.updateAdmin(administrators);
    }

}
