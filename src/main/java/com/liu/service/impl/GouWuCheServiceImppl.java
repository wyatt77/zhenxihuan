package com.liu.service.impl;

import com.liu.mapper.GouWuCheMapper;
import com.liu.pojo.Gouwuche;
import com.liu.service.GouWuCheService;
import com.liu.util.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("gouWuCheService")
@Transactional
public class GouWuCheServiceImppl implements GouWuCheService {
    @Resource
    private GouWuCheMapper gouWuCheMapper;
    @Override
    public int getSum() {
        return gouWuCheMapper.getSum();
    }

    @Override
    public List<Gouwuche> getGouWuChe(Integer startRowNum, Integer endRowNum) {
        List<Gouwuche> list=gouWuCheMapper.findGouWuChe(startRowNum,endRowNum);
        return list;
    }

    @Override
    public Page<Gouwuche> getGouWuCheByPage(Integer num, Integer count) {
        Integer pageNum =1;
        try{
            pageNum=Integer.valueOf(num);
        }catch (Exception e){
        }
        Integer pageCount =5;
        try {
            pageCount=Integer.valueOf(count);
        }catch (Exception e){

        }
        Page<Gouwuche> page=new Page<>();
        page.init(pageNum,pageCount,getSum());
        page.setList(getGouWuChe(page.getRowNum(),pageCount));
        return page;
    }
}
