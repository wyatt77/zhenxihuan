package com.liu.service.impl;

import com.liu.mapper.UsersMapper;
import com.liu.pojo.Users;
import com.liu.service.UsersService;
import com.liu.util.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("users")
public class UsersServiceImpl implements UsersService {
    @Resource
    private UsersMapper usersMapper;

    @Override
    public int getSum(String username,String role) {
        return usersMapper.getSum(username,role);
    }

    @Override
    public List<Users> getUsers(Integer startRowNum, Integer endRowNum,String username,String role) {
        List<Users> list = usersMapper.getUsers(startRowNum, endRowNum,username,role);
        for (Users users : list){
            System.out.println(users.toString());
        }
        return list;
    }

    @Override
    public Page<Users> getUsersByPage(Integer pageNum, Integer pageCount,String username,String role) {
        Integer num = 1;
        num = pageNum;
        Integer count = 10;
        count = pageCount;
        Page<Users> page = new Page<>();
        page.init(num,count,getSum(username,role));
        page.setList(getUsers(page.getRowNum(),count,username,role));
        return page;
    }

    @Override
    public Long addUsers(Users users) {
        if (users == null){
            return 0L;
        }
        long id = usersMapper.addUsers(users);
        return id;
    }

    @Override
    public Integer delUsersById(Integer id) {
        return usersMapper.delUsersById(id);
    }

    @Override
    public Long updateUsers(Users users) {
        return usersMapper.updateUsers(users);
    }
}
