package com.liu.service.impl;

import com.liu.pojo.Order;

import com.liu.mapper.OrderMapper;
import com.liu.pojo.OrderStatus;
import com.liu.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @program: zhenxihuan
 * @description
 * @author: zlr
 * @create: 2019-07-24 22:04
 **/
@Service
public class OrderServiceImpl implements OrderService {
@Autowired
private OrderMapper mapper;

    public void addorder(Order order) {
        Date date =new Date();
        //order.setOrderTime(date);
        mapper.insetOrder(order);
    }

    @Override
    public void delorder(Integer id) {
        mapper.delorder(id);
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        mapper.updateStatus(id,status);
    }

    @Override
    public List<Order> orderList() {
        return mapper.list();
    }

    @Override
    public List<OrderStatus> statusList() {
        return mapper.statusList();
    }
}
